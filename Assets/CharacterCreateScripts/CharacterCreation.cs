﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;

public class CharacterCreation : MonoBehaviour
{
    [SerializeField] private InputField nickname;
    [SerializeField] private InputField hairStyle;
    [SerializeField] private InputField hairColor;
    [SerializeField] private InputField eyesStyle;
    [SerializeField] private InputField eyesColor;
    [SerializeField] private InputField faceStyle;
    [SerializeField] private InputField skinColor;
    [SerializeField] private InputField gender;
    

    public GameManager gameManager;
    public ServerFeedback serverFeedback;
    public TemporaryData temporaryData;


    public void SendingHeroCreationRequest()
    {
        temporaryData = gameManager.ReturnDataFromTempFile();
        string sessionKey = temporaryData.sessionKey;
        Debug.Log(sessionKey);

        string heroNickname = nickname.text;
        string heroHairStyle = hairStyle.text;
        string heroHairColor = hairColor.text;
        string heroEyesStyle = eyesStyle.text;
        string heroEyesColor = eyesColor.text;
        string heroFaceStyle = faceStyle.text;
        string heroSkinColor = skinColor.text;
        string heroGender = gender.text;


        WWWForm HeroCreatorForm = new WWWForm();
        HeroCreatorForm.AddField("hero_create[Session_key]", sessionKey);
        HeroCreatorForm.AddField("hero_create[Nickname]", heroNickname);
        HeroCreatorForm.AddField("hero_create[Hairstyle]", heroHairStyle);
        HeroCreatorForm.AddField("hero_create[HairColor]", heroHairColor);
        HeroCreatorForm.AddField("hero_create[EyesStyle]", heroEyesStyle);
        HeroCreatorForm.AddField("hero_create[EyesColor]", heroEyesColor);
        HeroCreatorForm.AddField("hero_create[FaceStyle]", heroFaceStyle);
        HeroCreatorForm.AddField("hero_create[Gender]", heroGender);
        HeroCreatorForm.AddField("hero_create[SkinColor]", heroSkinColor);
        StartCoroutine(gameManager.Request(HeroCreatorForm, "hero/create"));
        StartCoroutine(GetObject());

    }

    public IEnumerator GetObject()
    {
        while (gameManager.done)
        {
            yield return new WaitForSeconds(0.1f);
        }
        serverFeedback = JsonUtility.FromJson<ServerFeedback>(gameManager.requestOutput);
        if(serverFeedback.errorNumber == 0)
        {
            Debug.Log("Success");
            //next scene
        }
    }


    public void CreateHero()
    {
        SendingHeroCreationRequest();
    }
}

