﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FormUtilities : MonoBehaviour
{

    //serializable editor objects
    [SerializeField] private InputField email;
    [SerializeField] private InputField password;
    [SerializeField] private InputField passwordConfirmation;
    [SerializeField] private Toggle rulesPrivacy;
    [SerializeField] private Button submitButton;
    public string emailText;
    public string passwordText;
    public GameManager gameManager;

    private void Update()
    {
        submitButton.interactable = Validate();
    }

    private bool Validate() 
    {
        emailText = email.text;
        passwordText = password.text;
        string passwordConfirmationTxt = passwordConfirmation.text;
        bool RulesPrivacyToggle = rulesPrivacy.isOn;

        if((string.IsNullOrEmpty(emailText)) || (string.IsNullOrEmpty(passwordText)) || (string.IsNullOrEmpty(passwordConfirmationTxt)) || (!RulesPrivacyToggle))
        {
            return false;
        }
        else if(passwordText != passwordConfirmationTxt)
        {
            return false;
        }
        else if(!gameManager.ValidateEmail(emailText))
        {
            return false;
        }
        return true;
    }
    public void RegisterUser() //Send registration
    { 
        WWWForm registerForm = new WWWForm();
        registerForm.AddField("account[email]", emailText);
        registerForm.AddField("account[password]", passwordText);
        StartCoroutine(gameManager.Request(registerForm, "register"));
        RegistrationFeedback registerFeedback = JsonUtility.FromJson<RegistrationFeedback>(gameManager.requestOutput); //receiving data from url
        Debug.Log(gameManager.requestOutput);

    }

    private class RegistrationFeedback // !!!!!!!!
    {
        public int errorNumber;
    }
}

