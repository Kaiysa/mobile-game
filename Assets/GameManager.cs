﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text.RegularExpressions;
using System;
using System.IO;

public class GameManager : MonoBehaviour
{
    public string requestOutput;
    public bool done = true;

    public IEnumerator Request(WWWForm post, string webURL) //collections method
    {
        done = true;
        using (UnityWebRequest wRequest = UnityWebRequest.Post("https://game.jurix.usermd.net/api/" + webURL, post)) //connecting with url
        {
            yield return wRequest.SendWebRequest();

            if (wRequest.isNetworkError || wRequest.isHttpError) //errors checking
            { 
            }
            else
            {  
                requestOutput = wRequest.downloadHandler.text; //receiving data from url
                done = false;
            }
        }
    }

    public bool ValidateEmail(string email)
    {
        const string MatchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
        + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

        if (email != null)
        {
            return Regex.IsMatch(email, MatchEmailPattern);
            
        }
        else
        {
            return false;
        }
            
    }

    public void SaveDataInTempFile(object temporaryData)
    {
        string tempData = JsonUtility.ToJson(temporaryData);
        File.WriteAllText(Application.dataPath + "/TemporaryData.json", tempData);
    }

    public TemporaryData ReturnDataFromTempFile()
    {
       string jsonString = File.ReadAllText(Application.dataPath + "/TemporaryData.json");
       TemporaryData temporaryData = JsonUtility.FromJson<TemporaryData>(jsonString);
       return temporaryData;

    }

}
[Serializable]
public class TemporaryData
{
    public string sessionKey;
}

[Serializable]
public class ServerFeedback
{
    public int errorNumber;
}