﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

public class UserLogin : MonoBehaviour
{
    public GameManager gameManager;

    //serializable editor objects
    [SerializeField] private InputField email;
    [SerializeField] private InputField password;
    [SerializeField] private Button submitButton;
    public string emailText;
    public string passwordText;

    public LoginFeedback loginfeed;
    public TemporaryData temporaryData;


    private void Update()
    {
        submitButton.interactable = Validate();
    }

    private bool Validate()
    {
        emailText = email.text;
        passwordText = password.text;

        if ((string.IsNullOrEmpty(emailText)) || (string.IsNullOrEmpty(passwordText))) 
        {
            return false;
        }
        else if (!gameManager.ValidateEmail(emailText))
        {
            return false;
        }
        return true;
    }

    public void LoginUser() //Send login request
    {

        WWWForm loginForm = new WWWForm();
        loginForm.AddField("account[email]", emailText);
        loginForm.AddField("account[password]", passwordText);
        StartCoroutine(gameManager.Request(loginForm, "login"));
        StartCoroutine(GetObject());

    }
    public IEnumerator GetObject()
    {
        while(gameManager.done)
        {
            yield return new WaitForSeconds(0.1f);
        }
        loginfeed = JsonUtility.FromJson<LoginFeedback>(gameManager.requestOutput);
        temporaryData.sessionKey = loginfeed.sessionKey;
        gameManager.SaveDataInTempFile(temporaryData);
        Debug.Log(temporaryData.sessionKey);
    }

}
[Serializable]
public class LoginFeedback // !!!!!!!!
{
    public int errorNumber;
    public string sessionKey;
}


